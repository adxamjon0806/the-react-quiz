import React from "react";
import { useQuiz } from "../context/QuizContext";

const Finished = () => {
  const { point, maxPossiblePoints, dispatch } = useQuiz();
  return (
    <>
      <h1>
        Finished with {point} out of {maxPossiblePoints}
      </h1>
      <button
        className="btn btn-ui"
        onClick={() => dispatch({ type: "restart" })}
      >
        Restart
      </button>
    </>
  );
};

export default Finished;
