import React from "react";
import { useQuiz } from "../context/QuizContext";

const Options = ({ questions }) => {
  const { dispatch, answer } = useQuiz();
  const hasAnswer = answer != null;
  console.log(questions);
  return (
    <div className="options">
      {questions.options.map((option, index) => (
        <button
          className={`btn btn-option ${answer === index ? "answer" : ""} ${
            hasAnswer
              ? index === questions.correctOption
                ? "correct"
                : "wrong"
              : ""
          }`}
          key={option}
          onClick={() => dispatch({ type: "newAnswer", payload: index })}
        >
          {option}
        </button>
      ))}
    </div>
  );
};

export default Options;
