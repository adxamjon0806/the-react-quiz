import React, { useEffect } from "react";
import { useQuiz } from "../context/QuizContext";

const Timer = () => {
  const { secondRemaining, dispatch } = useQuiz();
  let mins = Math.floor(secondRemaining / 60);
  let second = Math.floor(secondRemaining % 60);
  useEffect(() => {
    const id = setInterval(() => {
      //   console.log("tick");
      dispatch({ type: "tick" });
    }, 1000);
    return () => {
      clearInterval(id);
    };
  }, [dispatch]);
  return (
    <div className="timer">
      {mins} : {second}
    </div>
  );
};

export default Timer;
