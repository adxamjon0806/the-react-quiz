import Header from "./Header";
import Main from "./Main";
import "../index.css";
import Loader from "./Loader";
import StartScreen from "./StartScreen";
import Error from "./Error";
import Questions from "./Questions";
import Progres from "./Progres";
import NextButton from "./NextButton";
import Timer from "./Timer";
import Finished from "./Finished";
import { useQuiz } from "../context/QuizContext";

function App() {
  const { status } = useQuiz();

  return (
    <div className="app">
      <Header />
      <Main>
        {status === "loading" && <Loader />}
        {status === "error" && <Error />}
        {status === "ready" && <StartScreen />}
        {status === "active" && (
          <>
            <Progres />
            <Questions />
            <Timer />
            <NextButton />
          </>
        )}
        {status === "finish" && <Finished />}
      </Main>
    </div>
  );
}

export default App;
