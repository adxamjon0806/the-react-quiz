import React from "react";
import { useQuiz } from "../context/QuizContext";

const Progres = () => {
  const { numberOfQuestions, index, maxPossiblePoints, point } = useQuiz();
  return (
    <header className="progress">
      <progress max={numberOfQuestions} value={index} />
      <p>
        Questions <strong>{index + 1}</strong> / {numberOfQuestions}
      </p>
      <p>
        <strong>
          {point} / {maxPossiblePoints}
        </strong>
      </p>
    </header>
  );
};

export default Progres;
