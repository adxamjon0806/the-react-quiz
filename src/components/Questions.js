import React from "react";
import Options from "./Options";
import { useQuiz } from "../context/QuizContext";

const Questions = () => {
  const { questions, index } = useQuiz();
  return (
    <div>
      <h4>{questions[index].question}</h4>
      <Options questions={questions[index]} />
    </div>
  );
};

export default Questions;
