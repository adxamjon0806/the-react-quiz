import React from "react";
import { createContext, useEffect, useReducer, useContext } from "react";
import { Questions } from "./questions";

const SEC_FOR_QUESTIONS = 30;

const initialState = {
  questions: [],
  status: "loading",
  index: 0,
  point: 0,
  answer: null,
  secondRemaining: 10,
};

function reducer(state, action) {
  switch (action.type) {
    case "dataResive":
      return {
        ...state,
        questions: action.payload,
        status: "ready",
      };
    case "error":
      return {
        ...state,
        status: "error",
      };
    case "start":
      console.log(state);
      return {
        ...state,
        status: "active",
        secondRemaining: state.questions.length * SEC_FOR_QUESTIONS,
      };
    case "newAnswer":
      const question = state.questions.at(state.index);
      return {
        ...state,
        answer: action.payload,
        point:
          action.payload === question.correctOption
            ? state.point + question.points
            : state.point,
      };
    case "nextQuestions":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "finished":
      return {
        ...state,
        status: "finish",
      };
    case "restart":
      return {
        ...initialState,
        questions: state.questions,
        status: "ready",
      };
    case "tick":
      return {
        ...state,
        secondRemaining: state.secondRemaining - 1,
        status: state.secondRemaining === 0 ? "finish" : state.status,
      };
    default:
      throw new Error("This action is undefined");
  }
}

const QuizContext = createContext();

function QuizProvider({ children }) {
  const [
    { questions, status, index, point, answer, secondRemaining },
    dispatch,
  ] = useReducer(reducer, initialState);

  const numberOfQuestions = questions.length;
  let maxPossiblePoints = questions.reduce((prev, cur) => prev + cur.points, 0);
  useEffect(() => {
    dispatch({ type: "dataResive", payload: Questions });
  }, []);

  return (
    <QuizContext.Provider
      value={{
        questions,
        status,
        index,
        point,
        answer,
        secondRemaining,
        numberOfQuestions,
        maxPossiblePoints,
        dispatch,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
}

function useQuiz() {
  return useContext(QuizContext);
}

export { QuizProvider, useQuiz };
